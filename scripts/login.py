#!/usr/bin/python3
import requests
import hashlib
import json
import configparser
import sys

config = configparser.ConfigParser()
config.read(sys.argv[1])


userInfo = {
    "grant_type": "password",
    "username": config["USER"]["username"],
    "password": hashlib.md5(config["USER"]["password"].encode('utf-8')).hexdigest(),
    "client_id": "string"
    } 

connection = requests.post(config["webservice"]["url"]+config["BRAPI"]["token"], 
                           data=json.dumps(userInfo), 
                           headers=json.loads(config["BRAPI"]["headers"]))

authentication = connection.json()
print(authentication["access_token"])


