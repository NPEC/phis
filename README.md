# NPEC PHIS installation

For more information, please contact Sven Warris.

## Get the code
```{bash}
git clone https://git.wur.nl/NPEC/phis.git
cd phis/docker
```

## Installing components
 
First we need to install the mongo database and rdf4j. This installation process includes creating the needed volumes and network components. 

This will create 2 volumes to store the mongo database data and rdf4j data. These volumes will store all data and these can become large. Use for example a mount point to a large local or network storage device. 

Docker network 'backend' is also activated. Mongodb and rdf4j are part of this network.

### Configuration

Please open the docker-compose-components and edit the following parameters:

  - **MONGO_INITDB_ROOT_USERNAME**: username for the mongodb.
  - **MONGO_INITDB_ROOT_PASSWORD**: password for the mongodb. 
  - Replace *admin* (twice!) and *qwerty* in the **test:** line of the health check.
  - Volumes: adjust the local storage settings (now */data/*) and point to the proper locations.   

### Start the components

The components will be started by docker-compose. It might take some time to fully start-up, so it is best to do this a couple of minutes before starting the PHIS containers.

```{bash}
docker-compose -f docker-compose-components.yml up -d
```
 
### Accessing the docker containers

You can access the mongodb and rdf4j containers directly through docker:

```{bash}
docker exec -ti mongo /bin/bash
docker exec -ti rdf4j /bin/bash
```
  
## Installing PHIS configuration, web service and application

For this, several yaml files need to be edited.

### Docker-compose configuration

Open the *docker-compose-phis.yml*.

Edit the following parameters for the *phis-ws* service to create a user within PHIS with admin rights:
  
  - PHIS_WS_EMAIL
  - PHIS_WS_PASSWORD
  - PHIS_WS_FIRST_NAME
  - PHIS_WS_LAST_NAME

Edit the following parameters for the *phis-webapp* service:

  - PHIS_WEBAPP_HOST
  - PHIS_WS_URL
  - PHIS_WS_DOC_URL
  - PHIS_WS_APP_PATH
  
Currently, these point to aliases of localhost. Change these to the fully qualified host name or add *phis* and *phis-ws* to /etc/hosts:

```{bash}
127.0.1.1       phis phis-ws
```

This *docker-compose-phis.yml* also contains a volume. Change */data/ws* to a location on your system. Also, the frontend network is created in this file.

### PHIS configuration

Open the *phis-ws/opensilex.yml* and edit the following settings:

  - **ontologies**: change the *baseURI* to your local URI.
  - **big-data**: change the mongo database username and password to match the settings in *docker-compose-phis.yml*.
  
### Starting PHIS
  
```{bash}
docker-compose -f docker-compose-phis.yml up -d
```

### Access PHIS

PHIS should now be available on [localhost](http://phis:8889/phis-webapp/web/index.php).
 
Login with the credentials set in *docker-compose-phis.yml*.

### Access the containers

```{bash}
docker exec -ti phis-ws /bin/bash
docker exec -ti phis-webapp /bin/bash
```

## Firewalling

Docker-compose will create two networks. The components in these networks need to communicate with each other: rdf4j <- phis-ws for example. Also access to the mongodb and rdf4j needs to be restricted: public access is not recommended. Depending on your system, networking and firewalling, you need to adjust this accordingly. The following lines are some examples of CentOS firewalling which can be used to adjust the firewall.

```{bash}
sudo firewall-cmd --permanent --zone=trusted --add-interface=docker0
# check following interface names for frontend and backend networks using ip link show:
sudo firewall-cmd --permanent --zone=trusted --add-interface=br-68cc79d5fe8e
sudo firewall-cmd --permanent --zone=trusted --add-interface=br-c8c06a1295eb

# make sure phis-ws and phis can access each other
sudo firewall-cmd --permanent --zone=public --add-rich-rule='rule family=ipv4 source address=172.17.0.0/16 accept' && sudo firewall-cmd --reload

sudo firewall-cmd --permanent --zone=public --add-rich-rule='rule family=ipv4 source address=172.18.0.1/16 accept' && sudo firewall-cmd --reload

# drop all incoming traffic
sudo firewall-cmd --permanent --zone=public --set-target=DROP

# allow only http & ssh traffic
sudo firewall-cmd --zone=public --add-port=80/tcp --permanent
sudo firewall-cmd --zone=public --add-port=8080/tcp --permanent
sudo firewall-cmd --zone=public --add-port=22/tcp --permanent

# restart firewall and docker
sudo  service firewalld restart
sudo service docker restart

``` 


